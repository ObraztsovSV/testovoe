import React from 'react';
import './App.css';
import ApiService from './ApiService';
import Header from './components/header';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import Slider from "react-slick";

function SampleNextArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      className={className}
      onClick={onClick}>
      <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.0008 0C24.8212 0.00159992 31.9984 7.17884 32 16.0008C32 24.8228 24.8228 32 15.9992 32C7.17724 31.9984 0 24.8212 0 16.0008C0 7.17884 7.17724 0.00159992 16.0008 0ZM15.9992 28.8002C23.058 28.8002 28.8002 23.058 28.8002 16.0008C28.7986 8.94355 23.0564 3.20144 16.0008 3.19984C8.94195 3.20144 3.19984 8.94355 3.19984 16.0008C3.19984 23.0564 8.94195 28.7986 15.9992 28.8002Z" fill="#C9C9C9"/>
        <path d="M15.98 9.59302L22.3877 16.0007L15.98 22.4068V17.6006H9.58993V14.4008H15.98V9.59302Z" fill="#C9C9C9"/>
      </svg>
    </div>
    
  );
}

function SamplePrevArrow(props) {
  const { className, onClick } = props;
  return (
    <div
      className={className}
      onClick={onClick}>
        <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M15.9992 0C7.17884 0.00159992 0.00159992 7.17884 0 16.0008C0 24.8228 7.17724 32 16.0008 32C24.8228 31.9984 32 24.8212 32 16.0008C32 7.17884 24.8228 0.00159992 15.9992 0ZM16.0008 28.8002C8.94195 28.8002 3.19984 23.058 3.19984 16.0008C3.20144 8.94355 8.94355 3.20144 15.9992 3.19984C23.058 3.20144 28.8002 8.94355 28.8002 16.0008C28.8002 23.0564 23.058 28.7986 16.0008 28.8002Z" fill="#C9C9C9"/>
          <path d="M16.02 9.59302L9.6123 16.0007L16.02 22.4068V17.6006H22.4101V14.4008H16.02V9.59302Z" fill="#C9C9C9"/>
        </svg>
    </div>
  );
}
export default class App extends React.Component{
  apiService  = new ApiService();
  
  constructor(props) {
    super(props);
    this.state = {
      GalleryComponent: {},
      GridComponent: {},
      form: {}
    }
  }
  componentDidMount(){
    this.apiService
        .getData()
        .then((e)=>{
          this.setState({
            GalleryComponent: e.components[0].metadata,
            GridComponent: e.components[1].metadata.components,
            form: e.form
          })
        })
  }
  
  render(){

    const { GalleryComponent, GridComponent, form } = this.state;


    const settings = {
      className: "slider variable-width",
      dots: true,
      speed: 500,
      slidesToShow: GalleryComponent.slidesPerView,
      slidesToScroll: 2,
      initialSlide: 0,
      swipeToSlide: true,
      variableWidth: true,
      infinite: true,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: true,
            infinite: true,
          }
        },
        {
          breakpoint: 795,
          settings: {
            dots: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
          }
        },
        {
          breakpoint: 540,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 414,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
      ]
    };


    return (
      <div>
        <Header />
        <Container id="container">
          <Row>
            <Col><h1 className="gallaryTitle">{GalleryComponent.title}</h1></Col>
          </Row>
          <Row>
            <Col xs="12">
              <Slider {...settings}>
                <div style={{ width: 364 }}>
                  <img
                    width={364}
                    height={243}
                    src={GalleryComponent.images ? GalleryComponent.images[0] : ''}
                    alt="1 slide"
                  />
                </div>
                <div style={{ width: 364 }}>
                  <img
                    width={364}
                    height={243}
                    src={GalleryComponent.images ? GalleryComponent.images[1] : ''}
                    alt="2 slide"
                  />
                </div>
                <div style={{ width: 364 }}>
                  <img
                    width={364}
                    height={243}
                    src={GalleryComponent.images ? GalleryComponent.images[2] : ''}
                    alt="3 slide"
                  />
                </div>
                <div>
                  <img
                    width={364}
                    height={243}
                    src={GalleryComponent.images ? GalleryComponent.images[3] : ''}
                    alt="4 slide"
                  />
                </div>
                <div>
                  <img
                    width={364}
                    height={243}
                    src={GalleryComponent.images ? GalleryComponent.images[4] : ''}
                    alt="5 slide"
                  />
                </div>
                <div>
                  <img
                    width={364}
                    height={243}
                    src={GalleryComponent.images ? GalleryComponent.images[5] : ''}
                    alt="6 slide"
                  />
                </div>
                <div>
                 <img
                    width={364}
                    height={243}
                    src={GalleryComponent.images ? GalleryComponent.images[6] : ''}
                    alt="7 slide"
                  />
                </div>
              </Slider>
            </Col>
          </Row>

          <Row className="grid">
            <Col sm="12" className={GridComponent[0] ? 'col-' + GridComponent[0].col : ''}>
              <h2>{GridComponent[0] ? GridComponent[0].metadata.title : ''}</h2>
              {/* {GridComponent[0] ? GridComponent[0].metadata.text : ''}  Реакт будет воспринимать текст с сервера как строку и не прочтет теги*/} 
              <p>Структурный голод столь же важен для жизни, как и агрессия иллюстрирует объект, что лишний раз подтверждает правоту З.Фрейда. Субъект, как бы это ни казалось парадоксальным, непосредственно иллюстрирует архетип, Гоббс одним из первых осветил эту проблему с позиций психологии. Сознание выбирает конфликтный ассоцианизм. Реакция текстологически интегрирует филосовский аутизм.</p><p>Закон, согласно традиционным представлениям, начинает импульс. Проекция отталкивает тест одинаково по всем направлениям. Структурный голод столь же важен для жизни, как и коллективное бессознательное текстологически выбирает интеллект. Реакция отчуждает конформизм.</p>
            </Col>

            <Col sm="12" className={GridComponent[1] ? 'col-' + GridComponent[1].col : ''}>
              <h2>{GridComponent[1] ? GridComponent[1].metadata.title : ''}</h2>
              {/* {GridComponent[1] ? GridComponent[1].metadata.text : ''} аналагично */}
              <p>Абсолютная погрешность, в первом приближении, проецирует абстрактный интеграл по бесконечной области, что неудивительно. До недавнего времени считалось, что предел последовательности позиционирует анормальный криволинейный интеграл. Используя таблицу интегралов элементарных функций, получим: скалярное поле оправдывает интеграл по поверхности, дальнейшие выкладки оставим студентам в качестве несложной домашней работы. Подынтегральное выражение, конечно, накладывает абстрактный интеграл Пуассона. Уравнение в частных производных осмысленно поддерживает действительный определитель системы линейных уравнений.</p>
            </Col>
          </Row>

          <Row className="formTitle">
            <Col><h2>{form.title}</h2></Col>
          </Row>

            <Form>
              <Form.Row>
              <Col xs="12" md="6" lg="4">
                <Form.Group controlId="formGridName">
                  <Form.Label>{form.fields ? form.fields[0].label : 'text'}</Form.Label>
                  <Form.Control type={form.fields ? form.fields[0].type : ''}/>
                </Form.Group>
                <Form.Group controlId="formGridPhone">
                  <Form.Label>{form.fields ? form.fields[2].label : ''}</Form.Label>
                  <Form.Control type={form.fields ? form.fields[2].type : 'phone'} />
                </Form.Group>
              </Col>
                
              <Col xs="12" md="6" lg="4">
                <Form.Group controlId="formGridEmail">
                  <Form.Label>{form.fields ? form.fields[1].label : ''}</Form.Label>
                  <Form.Control type={form.fields ? form.fields[1].type : 'email'} />
                </Form.Group>
                <Form.Group controlId="formGridDate">
                  <Form.Label>{form.fields ? form.fields[3].label : ''}</Form.Label>
                  <Form.Control type={form.fields ? form.fields[3].type : 'date'} />
                </Form.Group>
              </Col>

              <Col sm="12" md="12" lg="4">
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Label>{form.fields ? form.fields[4].label + "(не обязательно)" : ''}</Form.Label>
                  <Form.Control as={form.fields ? form.fields[4].type : 'textarea'} />
                </Form.Group>
              </Col>
              </Form.Row>
              <Form.Row>
                <Col>
                <input id="checkbox" type="checkbox"/>
                <p id="checkboxText">Я даю согласие на обработку персональных данных согласно <a href='#'>политике конфиденциальности</a></p>
                </Col>
                
              </Form.Row>

              <Form.Row className="mb-5">
                <Col>
                  <Button className="submitBtn" type="submit">
                    Отправить заявку
                  </Button>
                </Col>
              </Form.Row>
            </Form>

        </Container>
        
      </div>
      
    )
  }
  
}
